# Ejercicio_Xml

A Partir de fichero XML Nutrición.xml obten la siguiente información:

1.**Listar información**: Mostrar el nombre de las comidas de las que tenemos información.

2.**Contar información**: Mostrar la cantidad de comidas de las que tenemos información.

3.**Buscar o filtrar información**: Pedir por teclado de una comida y mostrar el número de calorias que tiene.

4.**Buscar información relacionada**: Pedir por teclado un número de calorias y mostrar las comidas que tengan un número de calorias inferior

5.**Ejercicio libre**: Pedir por teclado una comida y mostrar las comidas que tengan mas proteinas que esa y mostrar el número de calorias.


