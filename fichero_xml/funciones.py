# Funcion 1 

from lxml import etree

def listar_informacion(datos):
    nombres=(datos.xpath("//food/name/text()"))
    return nombres

datos=etree.parse("Nutrición.xml")

#Funcion 2

def contar_informacion(datos):
    lista=[]
    for nombre in datos.xpath("/nutrition"):
        nombres=nombre.xpath('count(./food/name)')
        lista.append(int(nombres))
    return nombres

#Funcion 3

def buscar_informacion(datos,comida):
    comidas=datos.xpath('/nutrition/food[name="%s"]//calories/@total'%comida)
    return comidas

#Funcion 4

def informacion_relacionada(datos,calorias1):
    diccionario_comida={}
    lista_comida=datos.xpath("//food/name/text()")
    lista_calorias=datos.xpath("//food/calories/@total")
    lista_comida_calorias=[]
    lista_nombre=[]
    for comida,calorias in zip(lista_comida,lista_calorias):
        diccionario_comida['Comida']=comida
        diccionario_comida['Calorias']=calorias
        lista_comida_calorias.append(diccionario_comida)
        diccionario_comida={}
    for comida in lista_comida_calorias:
        if calorias1 > int(comida['Calorias']):
                lista_nombre.append(comida['Comida'])
    return lista_nombre

#Funcion 5

def ejercicio_libre(datos,comida1):
    
    lista_comida=datos.xpath("//food/name/text()")
    lista_calorias=datos.xpath("//food/calories/@total")
    lista_proteinas=datos.xpath("//food/protein/text()")
    proteinas=int(datos.xpath('//food[name="%s"]//protein/text()'%comida1)[0])
    lista_comida_calorias_proteinas=[]
    lista_nombre=[]
    for comida,calorias,proteinas in zip(lista_comida,lista_calorias,lista_proteinas):
        diccionario_comidas={}
        diccionario_comidas['Comida']=comida
        diccionario_comidas['Calorias']=calorias
        diccionario_comidas['Proteinas']=proteinas
        lista_comida_calorias_proteinas.append(diccionario_comidas)
        
    for comida in lista_comida_calorias_proteinas:
        if int(proteinas) < int(comida['Proteinas']):
                lista_nombre.append(comida['Comida'])
                lista_nombre.append(comida['Calorias'])
    return lista_nombre



