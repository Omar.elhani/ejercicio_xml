from lxml import etree
from funciones import listar_informacion,contar_informacion,buscar_informacion,informacion_relacionada,ejercicio_libre
datos=etree.parse("Nutrición.xml")

print(''' Menú
Es recomendable que listes la información primero para ver el nombre de las comidas

1. Listar información (Muestra el nombre de las comidas de las que tenemos información)
2. Contar infromación  (Muestra la cantidad de comidas de las que tenemos información)
3. Buscar o filtrar información (Pedir por teclado de una comida y mostrar el número de calorias que tiene)
4. Buscar información relacionada (Pedir por teclado un número de calorias y mostrar las comidas que tengan un número de calorias inferior)
5. Ejercicio Libre  (Pedir por teclado una comida y mostrar las comidas que tengan mas proteinas que esa y mostrar el número de calorias)
0. Salir''')

opcion=int(input("Introduzca que opción desea escoger:"))

while opcion!=0:

    if opcion<0 or opcion>5:
        print("Error, la opción es inválida.")
    
#Ejercicio 1

    if opcion==1:

        for nombre in listar_informacion(datos):
            print(nombre)

#Ejercicio 2

    if opcion==2:
        print(contar_informacion(datos))

#Ejercicio 3

    if opcion==3:

        nom_comida=input("Introduzca el nombre de la comida:")
        print("\n".join(buscar_informacion(datos,nom_comida)))

#Ejercicio 4

    if opcion==4:
        num_calorias1=int(input("Introduzca un número de calorias:"))
        print("\n".join(informacion_relacionada(datos,num_calorias1)))

#Ejercicio 5

    if opcion==5:
        comida2=input("Introduzca el nombre de la comida:")
        print("\n".join(ejercicio_libre(datos,comida2)))
    opcion=int(input("Introduzca que opción desea escoger:"))
print("Fin del programa")